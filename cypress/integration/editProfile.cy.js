describe('edit profile', () => {
  beforeEach(() => {
      cy.login('jackd@gmail.com', 'password');
    });

    it('edit', () => {
      cy.get('#ui-id-5').click();
      cy.get('#user_phone').type('+977-9845432199');
      cy.get('#user_address_attributes_city').type('Bhaktapur');
      cy.get('#user_address_attributes_county').type('Denmark');
      cy.contains('Update User').click();  
    });

  });