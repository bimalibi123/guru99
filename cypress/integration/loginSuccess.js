describe('login success', () => {

  it('register', () => {
    cy.visit('https://demo.guru99.com/insurance/v1/index.php');
    cy.contains('Register').click();
    cy.get('#user_title').select('Mr');
    cy.get('#user_firstname').type('Jack');
    cy.get('#user_surname').type('Daniel');
    cy.get('#user_phone').type('+977-9845432198');
    cy.get('#user_dateofbirth_1i').select('1985');
    cy.get('#user_dateofbirth_2i').select('October');
    cy.get('#user_dateofbirth_3i').select('28');
    cy.get('#licencetype_f').click();
    cy.get('#user_licenceperiod').select('5');
    cy.get('#user_occupation_id').select('Lawyer')
    cy.get('#user_address_attributes_street').type('Sundhara-7');
    cy.get('#user_address_attributes_city').type('Kathmandu');
    cy.get('#user_address_attributes_county').type('Nepal');
    cy.get('#user_address_attributes_postcode').type('4408');
    cy.get('#user_user_detail_attributes_email').type('jackd@gmail.com');
    cy.get('#user_user_detail_attributes_password').type('password');
    cy.get('#user_user_detail_attributes_password_confirmation').type('password')
    cy.get('[name="submit"]').click();
  });

  it('Login with registered email id and password', () => {
    cy.get('#email').type('jackd@gmail.com');
    cy.get('#password').type('password');
    cy.get('[type="submit"]').click();
    cy.contains('Signed in as');
  });
});