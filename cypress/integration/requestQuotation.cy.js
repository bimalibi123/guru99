describe('request quotation', () => {
  beforeEach(() => {
      cy.login('jackd@gmail.com', 'password');
    });

    it('request quotation form', () => {
        cy.get('#ui-id-2').click();
        cy.get('#quotation_breakdowncover').select('At home');
        cy.get('#quotation_windscreenrepair_f').click();
        cy.get('#quotation_incidents').type('My incident');
        cy.get('#quotation_vehicle_attributes_registration').type('1212');
        cy.get('#quotation_vehicle_attributes_mileage').type('55');
        cy.get('#quotation_vehicle_attributes_value').type('1234')
        cy.get('#quotation_vehicle_attributes_parkinglocation').select('Public Place');
        cy.get('#quotation_vehicle_attributes_policystart_1i').select('2020');
        cy.get('#quotation_vehicle_attributes_policystart_2i').select('March');
        cy.get('#quotation_vehicle_attributes_policystart_3i').select('20');
        cy.contains('Save Quotation').click();

      });
  });