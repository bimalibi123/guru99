describe('login failure', () => {

it('login with empty email field', () => {
  cy.visit('https://demo.guru99.com/insurance/v1/index.php');
  cy.get('#password').type('passworpazzd');
  cy.get('[type="submit"]').click();
  cy.contains('Enter your Email address and password correct');
  cy.get('#password').clear();
});

it('login with empty password field', () => {
  cy.get('#email').type('adminzaaa');
  cy.get('[type="submit"]').click();
  cy.contains('Enter your Email address and password correct');
  cy.get('#email').clear();
});

it('login with empty email and password field', () => {
  cy.get('[type="submit"]').click();
  cy.contains('Enter your Email address and password correct');
});

it('Fails login with wrong email or password', () => {
  cy.get('#email').type('newwwwww');
  cy.get('#password').type('eapfrg');
  cy.get('[type="submit"]').click();
  cy.contains('Enter your Email address and password correct');
});
});